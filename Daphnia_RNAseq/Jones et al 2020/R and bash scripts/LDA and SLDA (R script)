#Clear R's memory
rm(list=ls())
#In order to improve fit of linear discriminant analysis, we use clart and Klar to do
#stepwise LDA, which is a machine learning proceedure where multiple iterations of
#the model are run with different combinations of predictor variables and the best
#fitting predictor variables are kept.
#Load required packages
library(MASS)
library(tidyverse)
library(PredPsych)
library(caret)
library(klaR)

citation('pheatmap')
citation('ggplot2')
citation('caret')
citation('klaR')
#Read in raw datasets
LFC1000 <- read.csv('LFC Top 1000 matrix.csv')

#---------------------------------------------------------------------------------
#--------------------------------1------------------------------------------------
#Models with human-derived selection of genes
LDA1000 <- lda(Treatment~., data = LFC1000, CV = TRUE)
#Check the cross-validation accuracy of the model then refit without 'CV=TRUE' to 
#create the plots
confusionMatrix(LFC1000$Treatment, LDA1000$class)

#---------------------------------------------------------------------------------
#--------------------------------2------------------------------------------------
#Training model with stepwise LDA to optimise model predictors - this model uses
#leave one out cross validation, 24 folds (1 per replicate)

slda1000 <- train(Treatment ~ ., data = LFC1000,
                  method = "stepLDA",
                trControl = trainControl(method = "LOOcv", number=24))

slda1000b <- train(Treatment ~ DAPPUDRAFT_3267+DAPPUDRAFT_305070+DAPPUDRAFT_231626+
                     DAPPUDRAFT_220881+DAPPUDRAFT_210571+DAPPUDRAFT_67675+DAPPUDRAFT_306303+
                   DAPPUDRAFT_7769, data = LFC1000,
                  method = "stepLDA",
                  trControl = trainControl(method = "LOOcv", number=24))

#Ran model 10x, SLDAa is all genes produced by all model iterations (n=11) and SLDAb is 5 genes
#from 10 model iterations which appear in the most iterations
SLDA1000a <- lda(Treatment ~DAPPUDRAFT_220751+DAPPUDRAFT_317311+DAPPUDRAFT_3267+DAPPUDRAFT_305070+
                   DAPPUDRAFT_223101+DAPPUDRAFT_231626+DAPPUDRAFT_220881+DAPPUDRAFT_210571+
                   DAPPUDRAFT_67675+DAPPUDRAFT_7769,data = LFC1000)
confusionMatrix(LFC5alt$Treatment, LDA5a$class)
SLDA1000b <- lda(Treatment ~DAPPUDRAFT_220751+DAPPUDRAFT_317311+DAPPUDRAFT_223101+DAPPUDRAFT_67675+
                   DAPPUDRAFT_7769, data = LFC1000)
confusionMatrix(LFC1000$Treatment, SLDA1000b$class)
slda1000$finalModel
slda1000$predictors
slda1000
#---------------------------------------------------------------------------------
#--------------------------------3------------------------------------------------
#Plotting the models with 95% CI ellipses
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

#Create dataset for plot
SLDA1000adata<- cbind(LFC1000, predict(SLDA1000a)$x)
#Check the LD1 and LD2 relative contributions then alter the X and Y labels 
#accordingly
SLDA1000a
#Create plot with ellipises
SLDA1000aplot <- ggplot(SLDA1000adata, aes(LD1, LD2)) +
  geom_point(aes(color =Treatment, shape = Treatment)) +
  xlab('LD1(75.2%)') +
  ylab('LD2(20.5%)') +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black")) +
 #Add 95% CI ellipses to plot
   stat_ellipse(aes(x=LD1, y=LD2,color=Treatment),type = "norm") 
#Annotate plot with letter if combining into single figure
 
#---------------------------------------------------------------------------------
#Create dataset for plot
SLDA1000bdata<- cbind(LFC1000, predict(SLDA1000b)$x)
#Check the LD1 and LD2 relative contributions then alter the X and Y labels 
#accordingly
SLDA1000b
#Create plot with ellipises
SLDA1000bplot <- ggplot(SLDA1000bdata, aes(LD1, LD2)) +
  geom_point(aes(color =Treatment, shape = Treatment)) +
  xlab('LD1(64.3%)') +
  ylab('LD2(35.1%)') +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black")) +
  #Add 95% CI ellipses to plot
  stat_ellipse(aes(x=LD1, y=LD2,color=Treatment),type = "norm") 
